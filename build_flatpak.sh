#!/bin/bash
printf "\n\n---------------------------------------- FLATPAK BUILD --------------------------------------------\n";

# Aborts the script upon any faliure
set -e;

# Setup Script Variables
FLATPAK_REPO=$1;
FLATPAK_BUNDLE=$2;
_SCRIPT_FOLDER=$(realpath $(dirname $0));
_FLATHUB_REPO="flathub https://flathub.org/repo/flathub.flatpakrepo";
_FLATHUB_PACKAGES_TO_INSTALL="org.freedesktop.Platform//20.08 org.freedesktop.Sdk//20.08";
_FLATPAK_BUILD_FOLDER=build-dir;

# # work around some docker permission issues
# apt update
# apt install -y apt-utils debconf-utils dialog
# echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
# echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections


# Install build dependencies
printf "\nInstalling flatpak build dependencies\n";

apt update && apt install -y software-properties-common
add-apt-repository -y ppa:alexlarsson/flatpak
apt update && apt install -y flatpak-builder bzip2
flatpak remote-add --if-not-exists $_FLATHUB_REPO;
flatpak install -y flathub $_FLATHUB_PACKAGES_TO_INSTALL;

# Build Repo
printf "\nBuilding flatpak repository\n";
flatpak-builder --disable-rofiles-fuse --repo="$FLATPAK_REPO" "$_FLATPAK_BUILD_FOLDER" io.gitlab.librewolf-community.json;

# Build bundle
printf "\nBuilding flatpak bundle\n";
flatpak build-bundle $FLATPAK_REPO $FLATPAK_BUNDLE io.gitlab.librewolf-community master;

# Clean up flatpak files
printf "\nCleaning up flatpak related files\n";
rm -rf $_FLATPAK_BUILD_FOLDER;
rm -rf ./.flatpak-builder;
